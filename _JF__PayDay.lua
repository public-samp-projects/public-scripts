script_author('JustFedot')
script_name('[JF] PayDay')
script_version('1.0')
script_description('Хрень какая-то, но мне нравится.')


require("moonloader")
require ("sampfuncs")
local sampev = require("samp.events")
local encoding = require("encoding")
encoding.default = 'CP1251'
u8 = encoding.UTF8
local imgui = require("imgui")
local f = require 'moonloader'.font_flag
local font = renderCreateFont('Arial', 10, f.BOLD + f.SHADOW)


local MINIMAL_DEPOSIT = 272000000


function VioletTheme()
    local style = imgui.GetStyle()
    local colors = style.Colors
    local clr = imgui.Col
    local ImVec4 = imgui.ImVec4
    colors[clr.Text]                 = ImVec4(1.00, 1.00, 1.00, 1.00)
    colors[clr.TextDisabled]         = ImVec4(0.60, 0.60, 0.60, 1.00)
    colors[clr.WindowBg]             = ImVec4(0.09, 0.09, 0.09, 1.00)
    colors[clr.ChildWindowBg]        = ImVec4(9.90, 9.99, 9.99, 0.00)
    colors[clr.PopupBg]              = ImVec4(0.09, 0.09, 0.09, 1.00)
    colors[clr.Border]               = ImVec4(0.71, 0.71, 0.71, 0.40)
    colors[clr.BorderShadow]         = ImVec4(9.90, 9.99, 9.99, 0.00)
    colors[clr.FrameBg]              = ImVec4(0.34, 0.30, 0.34, 0.30)
    colors[clr.FrameBgHovered]       = ImVec4(0.22, 0.21, 0.21, 0.40)
    colors[clr.FrameBgActive]        = ImVec4(0.20, 0.20, 0.20, 0.44)
    colors[clr.TitleBg]              = ImVec4(0.52, 0.27, 0.77, 0.82)
    colors[clr.TitleBgActive]        = ImVec4(0.55, 0.28, 0.75, 0.87)
    colors[clr.TitleBgCollapsed]     = ImVec4(9.99, 9.99, 9.90, 0.20)
    colors[clr.MenuBarBg]            = ImVec4(0.27, 0.27, 0.29, 0.80)
    colors[clr.ScrollbarBg]          = ImVec4(0.08, 0.08, 0.08, 0.60)
    colors[clr.ScrollbarGrab]        = ImVec4(0.54, 0.20, 0.66, 0.30)
    colors[clr.ScrollbarGrabHovered] = ImVec4(0.21, 0.21, 0.21, 0.40)
    colors[clr.ScrollbarGrabActive]  = ImVec4(0.80, 0.50, 0.50, 0.40)
    colors[clr.ComboBg]              = ImVec4(0.20, 0.20, 0.20, 0.99)
    colors[clr.CheckMark]            = ImVec4(0.89, 0.89, 0.89, 0.50)
    colors[clr.SliderGrab]           = ImVec4(1.00, 1.00, 1.00, 0.30)
    colors[clr.SliderGrabActive]     = ImVec4(0.80, 0.50, 0.50, 1.00)
    colors[clr.Button]               = ImVec4(0.48, 0.25, 0.60, 0.60)
    colors[clr.ButtonHovered]        = ImVec4(0.67, 0.40, 0.40, 1.00)
    colors[clr.ButtonActive]         = ImVec4(0.80, 0.50, 0.50, 1.00)
    colors[clr.Header]               = ImVec4(0.56, 0.27, 0.73, 0.44)
    colors[clr.HeaderHovered]        = ImVec4(0.78, 0.44, 0.89, 0.80)
    colors[clr.HeaderActive]         = ImVec4(0.81, 0.52, 0.87, 0.80)
    colors[clr.Separator]            = ImVec4(0.42, 0.42, 0.42, 1.00)
    colors[clr.SeparatorHovered]     = ImVec4(0.57, 0.24, 0.73, 1.00)
    colors[clr.SeparatorActive]      = ImVec4(0.69, 0.69, 0.89, 1.00)
    colors[clr.ResizeGrip]           = ImVec4(1.00, 1.00, 1.00, 0.30)
    colors[clr.ResizeGripHovered]    = ImVec4(1.00, 1.00, 1.00, 0.60)
    colors[clr.ResizeGripActive]     = ImVec4(1.00, 1.00, 1.00, 0.89)
    colors[clr.CloseButton]          = ImVec4(0.33, 0.14, 0.46, 0.50)
    colors[clr.CloseButtonHovered]   = ImVec4(0.69, 0.69, 0.89, 0.60)
    colors[clr.CloseButtonActive]    = ImVec4(0.69, 0.69, 0.69, 1.00)
    colors[clr.PlotLines]            = ImVec4(1.00, 0.99, 0.99, 1.00)
    colors[clr.PlotLinesHovered]     = ImVec4(0.49, 0.00, 0.89, 1.00)
    colors[clr.PlotHistogram]        = ImVec4(9.99, 9.99, 9.90, 1.00)
    colors[clr.PlotHistogramHovered] = ImVec4(9.99, 9.99, 9.90, 1.00)
    colors[clr.TextSelectedBg]       = ImVec4(0.54, 0.00, 1.00, 0.34)
    colors[clr.ModalWindowDarkening] = ImVec4(0.20, 0.20, 0.20, 0.34)
end
VioletTheme()


local Ecfg = {
	_VERSION	= "2.0.4",
	_AUTHOR		= "Double Tap Inside",
	_EMAIL		= "double.tap.inside@gmail.com"
}

function Ecfg.__init()
	local self = {}
	
	-- \a => '\\a', \0 => '\\0', 31 => '\31'
	local shortControlCharEscapes = {
	  ["\a"] = "\\a",  ["\b"] = "\\b", ["\f"] = "\\f", ["\n"] = "\\n",
	  ["\r"] = "\\r",  ["\t"] = "\\t", ["\v"] = "\\v"
	}
	
	local longControlCharEscapes = {} -- \a => nil, \0 => \000, 31 => \031
	
	local function escape(str)
	  return (str:gsub("\\", "\\\\")
				 :gsub("(%c)%f[0-9]", longControlCharEscapes)
				 :gsub("%c", shortControlCharEscapes))
	end
	
	local function draw_string(str)
		return string.format("%q", escape(str))
	end

	local function draw_key(key)
		if "string" == type(key) and key:match("^[_%a][_%a%d]*$") then
			return key
			
		elseif "number" == type(key) then
			return "["..key.."]"

		else
			return "["..draw_string(key).."]"
		end
	end
	
	local function draw_table(tbl, tab)
		local tab = tab or ""
		local result = {}
		
		for key, value in pairs(tbl) do
			if type(value) == "string" then
				if type(key) == "number" and key <= #tbl then
					table.insert(result, draw_string(value))
					
				else
					table.insert(result, draw_key(key).." = "..draw_string(value))
				end
				
			elseif type(value) == "number" or type(value) == "boolean" then
				if type(key) == "number" and key <= #tbl then
					table.insert(result, tostring(value))
					
				else
					table.insert(result, draw_key(key).." = "..tostring(value))
				end
			
			elseif type(value) == "table" then
				if type(key) == "number" and key <= #tbl then
					table.insert(result, draw_table(value, tab.."\t"))
					
				else
					table.insert(result, draw_key(key).." = "..draw_table(value, tab.."\t"))
				end
				
			else
				if type(key) == "number" and key <= #tbl then
					table.insert(result, draw_string(tostring(value)))
					
				else
					table.insert(result, draw_key(key).." = "..draw_string(tostring(value)))
				end
			end
		end
		
		if #result == 0 and tab == "" then
			return ""
			
		elseif #result == 0 then
			return "{}"
		
		elseif tab == "" then
			return table.concat(result, ",\n")..",\n"
		
		else
			return "{\n"..tab..table.concat(result, ",\n"..tab)..",\n"..tab:sub(2).."}"
		end       
	end
	
	local function draw_value(value, tab)
		if type(value) == "string" then
			return draw_string(value)
		
		elseif type(value) == "number" or type(value) == "boolean" or type(value) == "nil" then
			return tostring(value)
		
		elseif type(value) == "table" then
			return draw_table(value, tab)
			
		else
			return draw_string(tostring(value))
		end
	end
	
	local function draw_list(list)
		local result = {}
	
		for index, value in ipairs(list) do
			table.insert(result, "table.insert(list, "..draw_value(value, "\t")..")")
		end
		
		if #result == 0 then
			return ""
			
		else
			return table.concat(result, "\n").."\n"
		end
	end
	
	function self.list_load(filename, save)
		assert(type(filename)=="string", ("bad argument #1 to 'load' (string expected, got %s)"):format(type(filename)))
		
		local file = io.open(filename, "r")
		
		if file then
			local text = file:read("*all")
			file:close()
			local lua_code = loadstring("local list = {}\n"..text.."\nreturn list")
			
			if lua_code then
				local result = lua_code()
				
				if type(result) == "table" then
					if save then
						self.list_save(filename, result)
					end
					
					return result
				end
			end
		end
	end
	
	function self.list_save(filename, new)
		assert(type(filename)=="string", ("bad argument #1 to 'list_save' (string expected, got %s)"):format(type(filename)))
		assert(type(new)=="table", ("bad argument #2 to 'list_save' (table expected, got %s)"):format(type(new)))
	
		self.mkpath(filename)
		local file = io.open(filename, "w+")
		
		if file then
			local text = draw_list(new)
			file:write(text)
			file:close()
			
			return true
		else
			return false
		end
	end
	
	function self.list_insert(filename, index, value)
		assert(type(filename)=="string", ("bad argument #1 to 'list_insert' (string expected, got %s)"):format(type(filename)))
		
		if value then
			assert(type(index)=="number", ("bad argument #2 to 'list_insert' (number expected, got %s)"):format(type(index)))
		end
		
		local result
		
		if value then
			result = "table.insert(list, "..index..", "..draw_value(value, "\t")..")"
			
		else
			result = "table.insert(list, "..draw_value(index, "\t")..")"
		end
		
		self.mkpath(filename)
		local file = io.open(filename, "a+")
		
		if file then
			file:write(result.."\n")
			file:close()
			return true
			
		else
			return false
		end
	end
	
	function self.list_remove(filename, index)
		assert(type(filename)=="string", ("bad argument #1 to 'list_remove' (string expected, got %s)"):format(type(filename)))
		assert(type(index)=="number" or index == nil, ("bad argument #2 to 'list_remove' (number or nil expected, got %s)"):format(type(index)))
		
		local result
		
		if index then
			result = "table.remove(list, "..index..")"
			
		else
			result = "table.remove(list)"
		end
		
		self.mkpath(filename)
		local file = io.open(filename, "a+")
		
		if file then
			file:write(result.."\n")
			file:close()
			return true
			
		else
			return false
		end
		
	end
	
	function self.list_set(filename, index, value)
		assert(type(filename)=="string", ("bad argument #1 to 'list_set' (string expected, got %s)"):format(type(filename)))
		assert(type(index)=="number", ("bad argument #2 to 'list_set' (number expected, got %s)"):format(type(index)))
		
		local result = "list["..index.."] = "..draw_value(value, "\t")
		
		self.mkpath(filename)
		local file = io.open(filename, "a+")
		
		if file then
			file:write(result.."\n")
			file:close()
			return true
			
		else
			return false
		end
	end
	
	function self.mkpath(filename)
		assert(type(filename)=="string", ("bad argument #1 to 'mkpath' (string expected, got %s)"):format(type(filename)))
	
		local sep, pStr = package.config:sub(1, 1), ""
		local path = filename:match("(.+"..sep..").+$") or filename
		
		for dir in path:gmatch("[^" .. sep .. "]+") do
			pStr = pStr .. dir .. sep
			createDirectory(pStr)
		end
	end
	
	function self.load(filename, save)
		assert(type(filename)=="string", ("bad argument #1 to 'load' (string expected, got %s)"):format(type(filename)))
	
		local file = io.open(filename, "r")
		
		if file then 	
			local text = file:read("*all")
			file:close()
			local lua_code = loadstring("return {"..text.."}")
			
			if lua_code then
				local result = lua_code()
				
				if type(result) == "table" then
					if save then
						self.save(filename, result)
					end
					
					return result
				end
			end
		end
	end
	
	function self.save(filename, new)
		assert(type(filename)=="string", ("bad argument #1 to 'save' (string expected, got %s)"):format(type(filename)))
		assert(type(new)=="table", ("bad argument #2 to 'save' (table expected, got %s)"):format(type(new)))
	
		self.mkpath(filename)
		local file = io.open(filename, "w+")
		
		if file then
			local text = draw_table(new)
			file:write(text)
			file:close()
			
			return true
		else
			return false
		end
	end
	
	function self.append(filename, value)
		assert(type(filename)=="string", ("bad argument #1 to 'append' (string expected, got %s)"):format(type(filename)))
		
		self.mkpath(filename)
		local file = io.open(filename, "a+")
		
		if file then
			file:write(draw_value(value, "\t")..",\n")
			file:close()
			
			return true
		else
			return false
		end
	end
	
	function self.set(filename, key, value)
		assert(type(filename)=="string", ("bad argument #1 to 'set' (string expected, got %s)"):format(type(filename)))
		assert(type(key)=="string" or type(key)=="number", ("bad argument #2 to 'set' (string or number expected, got %s)"):format(type(key)))
		
		self.mkpath(filename)
		local file = io.open(filename, "a+")
		
		if file then		
			file:write("\n"..draw_key(key).." = "..draw_value(value)..",")
			file:close()
			
			return true
		else
			return false
		end
	end
	
	function self.update(old, new, overwrite)
		assert(type(old)=="table", ("bad argument #1 to 'update' (table expected, got %s)"):format(type(old)))
		assert(type(new)=="string" or type(new)=="table", ("bad argument #2 to 'update' (string or table expected, got %s)"):format(type(new)))
		
		if overwrite == nil then
			overwrite = true
		end
	
		if type(new) == "table" then
			if overwrite then
				for key, value in pairs(new) do
					old[key] = value
				end
				
			else
				for key, value in pairs(new) do
					if not old[key] then
						old[key] = value
					end
				end
			end
			
			return true
			
		elseif type(new) == "string" then
			local loaded = self.load(new)
			
			if loaded then
				if overwrite then
					for key, value in pairs(loaded) do
						old[key] = value
					end
					
				else
					for key, value in pairs(loaded) do
						if not old[key] then
							old[key] = value
						end
					end
				end
				
				return true
			end
		end
		
		return false
	end
	
	return self
end

setmetatable(Ecfg, {
	__call = function(self)
		return self.__init()
	end
})

local ecfg = Ecfg()

local filename = getWorkingDirectory()..'\\config\\'..thisScript().name..'\\Config.zalupa'

function defaultConfig()
    local config = {
        settings = {
            pin = 0,
        },
        log = {},
		currentDeposit = 0,
		moneyPerHour = 0,
		currentMain = 0,
		zarplataPerhour = 0,
    }
    return config
end
local config
config = ecfg.load(filename)
if not config then config = defaultConfig() ecfg.save(filename,config) end
function save() ecfg.save(filename,config) end

local imgui_config = {
    pin = imgui.ImInt(0),
}
imgui_config.pin.v = config.settings.pin

local status = {
    on = false,
    go = false,
}

function randomInRange(from, to)
    math.randomseed(os.time()) -- инициализируем генератор случайных чисел текущим временем
    return math.random(from, to) -- генерируем случайное число в диапазоне от from до to
end

local main_thread = lua_thread.create_suspended(function()

    if not checkBankInterior() then
        off()
        addChat('{ff0000}Ошибка!{ffffff} Вы не в здании банка.')
    end
    local passedTime, timer = (randomInRange(5,15)*60), os.clock()
    if status.on and status.go then
        repeat
            wait(0)
        until ((os.clock() - timer) >= passedTime) or not status.on or not status.go
        repeat
            wait(300)
            setGameKeyState(10,255)
        until not status.on or not status.go or not checkBankInterior()
        if not checkBankInterior() then
            off()
            addChat('{ff0000}Ошибка!{ffffff} Вы не в здании банка.')
        end
    end
    return

end)

local dp = {
	on = false,
	summ = 0,
}

function off()
    status = {
        on = false,
        go = false,
    }
    main_thread:terminate()
end
  

function sampev.onServerMessage(color, text)

    if text:find('^Депозит в банке. %$%d+') then
        local date,money = getCurrentDate(),tonumber(text:match('^Депозит в банке. %$(%d+)'))
		config.moneyPerHour = money
        for k,v in pairs(config.log) do
            if v[1] == date then
                v[2] = v[2] + money
                save()
                return
            end
        end
        table.insert(config.log,1,{date,money})
        save()
	elseif text:find('^Текущая сумма на депозите. %$%d+$') then
		config.currentDeposit = tonumber(text:match('^Текущая сумма на депозите. %$(%d+)$'))
		save()
	elseif text:find('^Сумма к выплате: %$%d+$') then

		local date,money = getCurrentDate(),tonumber(text:match('^Сумма к выплате: %$(%d+)$'))
		config.zarplataPerhour = money
        for k,v in pairs(config.log) do
            if v[1] == date then
                v[2] = v[2] + money
                save()
                return
            end
        end
        table.insert(config.log,1,{date,money})
        save()
	elseif text:find('^Текущая сумма в банке: %$%d+$') then
		config.currentMain = tonumber(text:match('^Текущая сумма в банке: %$(%d+)$'))
		save()
	elseif text:find('^Вы сняли деньги с депозитного счета %$%d+%. Комиссия: %$%d+') then
		config.currentDeposit = config.currentDeposit - (tonumber(text:match('^Вы сняли деньги с депозитного счета %$(%d+)%. Комиссия: %$%d+')) + tonumber(text:match('^Вы сняли деньги с депозитного счета %$%d+%. Комиссия: %$(%d+)')))
		save()
	elseif text:find('^%[Информация%] {FFFFFF}Вы сняли со своего банковского счета %$%d+') then
		local b = text:match('%$(%d+)')
		config.currentMain = config.currentMain-tonumber(b)
		save()
	elseif text:find('^%[Информация%] {FFFFFF}Вы положили на свой банковский счет %$%d+') then
		local b = text:match('%$(%d+)')
		config.currentMain = config.currentMain+tonumber(b)
		save()
	elseif text:find('^{FFFFFF}Состояние счета: {......}%$%d+,{FFFFFF} состояние депозита: {......}%$%d+') then
		local a,b = text:match('^{FFFFFF}Состояние счета: {......}%$(%d+),{FFFFFF} состояние депозита: {......}%$(%d+)')
		config.currentDeposit,config.currentMain = tonumber(b),tonumber(a)
		save()
	elseif text:find('^%[Информация%] {FFFFFF}Вы перевели %$%d+ игроку .+ на счет') then
		local b = text:match('%$(%d+)')
		config.currentMain = config.currentMain-tonumber(b)
		save()
	elseif text:find('^Вам поступил перевод на ваш счет в размере %$%d+ от жителя') then
		local b = text:match('%$(%d+)')
		config.currentMain = config.currentMain+tonumber(b)
		save()
    end

    if status.on then

        if not checkBankInterior() then
            off()
            addChat('{ff0000}Ошибка!{ffffff} Вы не в здании банка.')
            return
        end
        if text:find('^__________Банковский чек__________') then
            status.go = true
            main_thread:run()
        elseif text:find('^Класть деньги на депозит, можно 1%-ин раз в час!$') then
            --status.go = true
            main_thread:run()
        elseif text:find('^Вы положили на свой депозитный счет %$%d+$') then
            status.go = false
        end

    end

end

function getCurrentDate()
    local currentDate = os.date("*t")
    return string.format("%02d.%02d.%d", currentDate.day, currentDate.month, currentDate.year)
end





function sampev.onShowDialog(id, style, title, button1, button2, text)

	if title == '{BFBBBA}' then
		if text:find('{929290}Вы должны подтвердить свой PIN%-код к карточке%.') then
			if #tostring(config.settings.pin) >= 4 then
				sampSendDialogResponse(id,1,false,config.settings.pin)
				return false
			end
		elseif text:find('{FFFFFF}PIN%-код принят!') and text:find('Хорошего {DBEF2A}дня!') then
			sampSendDialogResponse(id,1,false,false)
			return false
		end
	end

    if status.on and status.go then

        if not checkBankInterior() then
            off()
            addChat('{ff0000}Ошибка!{ffffff} Вы не в здании банка.')
            return
        end
        if title:find('{BFBBBA}Банк') and text:find('Пополнить депозит') then
            n = 0
            for line in text:gmatch("[^\r\n]+") do
                if line:find('Пополнить депозит') then
                    sampSendDialogResponse(id,1,n,false)
                    return false
                end
                n=n+1
            end
        elseif title:find('{BFBBBA}Введите сумму') and text:find('{FFFFFF}Введите сумму от {B7D22C}%$%d+ до %$%d+{FFFFFF} для пополнения депозита') then
			sampSendDialogResponse(id,1,false,'10000000')
            --status.go = false
            return false
        end
    end


	if dp.on then
		if not checkBankInterior() then
            off()
            addChat('{ff0000}Ошибка!{ffffff} Вы не в здании банка.')
            return
        end
        if title:find('{BFBBBA}Банк') and text:find('Пополнить депозит') then
            n = 0
            for line in text:gmatch("[^\r\n]+") do
                if line:find('Снять деньги с депозита') then
                    sampSendDialogResponse(id,1,n,false)
                    return false
                end
                n=n+1
            end
        elseif title:find('{BFBBBA}Введите сумму') and text:find('{FFFFFF}Введите сумму от {B7D22C}.+ до .+{FFFFFF} для получения денег со своего депозита') then
			if dp.summ >= 10000000 then
				sampSendDialogResponse(id,1,false,'10000000')
				dp.summ = dp.summ-10000000
				if dp.summ == 0 then
					dp.on = false
					addChat('Вы сняли необходимую сумму!')
					return
				end
				openBankMenu()
				return false
			elseif dp.summ <= 10000000 and dp.summ >= 10000 then
				sampSendDialogResponse(id,1,false,tostring(dp.summ))
				dp.on = false
				addChat('Вы сняли необходимую сумму!')
				return false
			else
				addChat('Ошибка! Сумма не может быть менее {99ff99}10.000$')
				dp.on = false
				return false
			end
        end
	end

end

function addChat(a)
    sampAddChatMessage('{ffa500}'..thisScript().name..'{ffffff}: '..a, -1)
end

local imgui_windows = {
    main = imgui.ImBool(false),
}



function openBankMenu()
	lua_thread.create(function()
		repeat
			wait(300)
			setGameKeyState(10,255)
		until not checkBankInterior() or not dp.on or sampIsDialogActive()
		return
	end)
end

function main()
    repeat wait(0) until isSampAvailable()
    while not isSampLoaded() do wait(0) end
    sampRegisterChatCommand('pd', function() imgui_windows.main.v = not imgui_windows.main.v end)
    sampRegisterChatCommand('pd_clear', function() config.log = {} save() addChat('Логи {99ff99}успешно{ffffff} очищены.') end)
	sampRegisterChatCommand('pd_take', function(a)
		if dp.on then dp.on = false addChat('Отключено.') return end
		if not a:find('^%d+$') then addChat('Ошибка. Правильное использование: {ffa500}/pd_take *сумма*') return end
		dp.summ=tonumber(a)
		if dp.summ < 10000 then addChat('Ошибка! Сумма не может быть менее {99ff99}10.000$') addChat('Либо оно не кратное 10.000.') dp.on = false return end
		if ((dp.summ)%10000) ~= 0 then addChat('Сумма должна быть кратной 10.000. Пример: 10.000.000, 13.500.000.') dp.on = false return end
		if config.currentDeposit < (dp.summ+((dp.summ/100)*7)) then
			addChat('Ошибка. Вашей суммы на депозите не хватает для снятия.')
			addChat('Что-бы снять всё под ноль введите сумму: {99ff99}'..formatMoney(config.currentDeposit-((config.currentDeposit/100)*7))..'$')
			dp.on = false
			return
		end
		dp.on = true
		openBankMenu()
		addChat('Снимаем бабки: {99ff99}'..formatMoney(dp.summ)..'$')
		addChat('С комиссией уйдёт: {99ff99}~'..formatMoney(dp.summ+((dp.summ/100)*7))..'$')
		addChat('Напоминаем, нужно стоять у кассы.')
	end)
    addChat('Скрипт загружен. {ffa500}/pd')
    addChat('Для очистки логов пропишите: {ffa500}/pd_clear')
	addChat('Для снятия депозита пропишите: {ffa500}/pd_take')
    while true do wait(0)
        imgui.Process = imgui_windows.main.v
        if status.on then
            local PosX, PosY = getStructElement(getStructElement(sampGetInputInfoPtr(), 0x8, 4), 0x8, 4), getStructElement(getStructElement(sampGetInputInfoPtr(), 0x8, 4), 0xC, 4) - 15
            renderFontDrawText(font, '{ffa500}'..thisScript().name..': {ffffff}работает!', PosX, PosY + 80, 0xFFFFFFFF, 0x90000000)
        end
    end
end

function imgui.OnDrawFrame()

    local w,h = getScreenResolution()
    
    if imgui_windows.main.v then
    
        imgui.SetNextWindowSize(imgui.ImVec2(350,440), imgui.Cond.FirstUseEver)
        imgui.SetNextWindowPos(imgui.ImVec2(w/2-350/2, h/2-440/2), imgui.Cond.FirstUseEver)
        imgui.Begin(u8(thisScript().name)..' Ver: '..thisScript().version.." ##main_window", imgui_windows.main, imgui.WindowFlags.NoCollapse)

        -- imgui.SetCursorPosX((imgui.GetWindowWidth() - 150) / 2)
        -- if imgui.ButtonActivated(status.on,u8'Запуск', imgui.ImVec2(150,25)) then
        --     if checkBankInterior() then
        --         status.on = not status.on
        --         if status.on then
        --             --status.on = true
        --             --status.go = true
        --             --main_thread:run()
        --             addChat('Запущен.')
        --         else
        --             off()
        --             addChat('Остановлен.')
        --         end
        --     else
        --         off()
        --         addChat('{ff0000}Ошибка!{ffffff} Вы не в здании банка.')
        --     end
        -- end
		-- imgui.SetCursorPosX((imgui.GetWindowWidth() - 120) / 2)
		-- imgui.PushItemWidth(120)
		-- if imgui.InputInt('##pin_int', imgui_config.pin,0,0) then
		-- 	config.settings.pin = imgui_config.pin.v
		-- 	save()
		-- end
		-- imgui.PopItemWidth()
		-- if imgui.IsItemHovered() then imgui.SetTooltip(u8('Введите ваш пин код в это поле.')) end

        imgui.BeginChild('##main_child', imgui.ImVec2(0,0), true)
			imgui.SetCursorPosX((imgui.GetWindowWidth() - (imgui.CalcTextSize("->O<-").x+imgui.GetStyle().ItemSpacing.x*2)))
			showAllMoney()
			imgui.SetCursorPosY(imgui.GetStyle().ItemSpacing.y)
			imgui.TextColoredRGB('Основной счёт: {99ff99}'..formatMoney(config.currentMain)..'$')
			if imgui.IsItemHovered() then
				imgui.BeginTooltip()
					imgui.TextColoredRGB('Доход в час: {99ff99}'..formatMoney(config.zarplataPerhour)..'$')
					imgui.TextDisabled(u8('24ч: '..formatMoney(config.zarplataPerhour*24)..'$'))
					imgui.TextDisabled(u8('1мес: '..formatMoney(config.zarplataPerhour*24*30)..'$'))
				imgui.EndTooltip()
			end
			imgui.TextColoredRGB('Депозит: {99ff99}'..formatMoney(config.currentDeposit)..'$')
			if imgui.IsItemHovered() then
				imgui.BeginTooltip()
					imgui.TextColoredRGB('Доход в час: {99ff99}'..formatMoney(config.moneyPerHour)..'$')
					imgui.TextDisabled(u8('24ч: '..formatMoney(config.moneyPerHour*24)..'$'))
					imgui.TextDisabled(u8('1мес: '..formatMoney(config.moneyPerHour*24*30)..'$'))
				imgui.EndTooltip()
			end
			imgui.TextColoredRGB('Можно снять: {ABCDEF}'..formatMoney(config.currentDeposit-MINIMAL_DEPOSIT+config.currentMain)..'$')
			if imgui.IsItemHovered() then
				imgui.BeginTooltip()
					imgui.TextColoredRGB('Со счёта: {99ff99}'..formatMoney(config.currentMain)..'$')
					imgui.TextColoredRGB('С депозита: {99ff99}'..formatMoney(config.currentDeposit-MINIMAL_DEPOSIT)..'$')
				imgui.EndTooltip()
			end
			imgui.Separator()
			imgui.BeginChild('##log_child', imgui.ImVec2(-1, -1), false)
				for k,v in ipairs(config.log) do
					imgui.TextColoredRGB('{ffa500}['..v[1]..']{ffffff} Заработали: {99ff99}'..formatMoney(v[2])..'$')
					imgui.Separator()
				end
			imgui.EndChild()

        imgui.EndChild()

        imgui.End()

    end

end

function showAllMoney()
	imgui.Button('->O<-', imgui.ImVec2(0, 25))
	if imgui.IsItemHovered() then
		imgui.BeginTooltip()
			imgui.TextColoredRGB('1ч: {99ff99}'..formatMoney(config.moneyPerHour+config.zarplataPerhour)..'$')
			imgui.TextColoredRGB('24ч: {99ff99}'..formatMoney(config.moneyPerHour*24+config.zarplataPerhour*24)..'$')
			imgui.TextColoredRGB('1мес: {99ff99}'..formatMoney(config.moneyPerHour*24*30+config.zarplataPerhour*24*30)..'$')
		imgui.EndTooltip()
	end
end

function simplifiedSumm(text)
    local K = 1000
    local M = 1000000
    local B = 1000000000

    local function round(num, numDecimalPlaces)
        local mult = 10^(numDecimalPlaces or 0)
        local roundedNum = math.floor(num * mult + 0.5) / mult

        -- Удаляем .0, если оно присутствует
        local roundedStr = tostring(roundedNum)
        if roundedStr:find("%.0$") then
            roundedStr = roundedStr:gsub("%.0$", "")
        end
        return roundedStr
    end

    local function simplify_number(number)
        local suffix, value
        if number >= B then
            suffix = "kkk"
            value = round(number / B, 3)
        elseif number >= M then
            suffix = "kk"
            value = round(number / M, 3)
        elseif number >= K then
            suffix = "k"
            value = round(number / K, 1)
        else
            return tostring(number)
        end

        return value .. suffix
    end
	return simplify_number(text)
end

function reverseTable(t)
    local reversedTable = {}
    local itemCount = #t
    for k, v in ipairs(t) do
        reversedTable[itemCount + 1 - k] = v
    end
    return reversedTable
end

function formatMoney(num)
    if not num then return "0" end
    return string.format("%d", num):reverse():gsub("(%d%d%d)", "%1,"):reverse():gsub("^,", "")
end

function imgui.ButtonActivated(activated, ...)
    if activated then
        imgui.PushStyleColor(imgui.Col.Button, imgui.GetStyle().Colors[imgui.Col.CheckMark])
        imgui.PushStyleColor(imgui.Col.ButtonHovered, imgui.GetStyle().Colors[imgui.Col.CheckMark])
        imgui.PushStyleColor(imgui.Col.ButtonActive, imgui.GetStyle().Colors[imgui.Col.CheckMark])

        if imgui.Button(...) then
            imgui.PopStyleColor()
            imgui.PopStyleColor()
            imgui.PopStyleColor()
            return true
        end

        imgui.PopStyleColor()
        imgui.PopStyleColor()
        imgui.PopStyleColor()

    else
        return imgui.Button(...)
    end
end

function imgui.TextColoredRGB(text)
    local style = imgui.GetStyle()
    local colors = style.Colors
    local ImVec4 = imgui.ImVec4

    local explode_argb = function(argb)
        local a = bit.band(bit.rshift(argb, 24), 0xFF)
        local r = bit.band(bit.rshift(argb, 16), 0xFF)
        local g = bit.band(bit.rshift(argb, 8), 0xFF)
        local b = bit.band(argb, 0xFF)
        return a, r, g, b
    end

    local getcolor = function(color)
        if color:sub(1, 6):upper() == 'SSSSSS' then
            local r, g, b = colors[1].x, colors[1].y, colors[1].z
            local a = tonumber(color:sub(7, 8), 16) or colors[1].w * 255
            return ImVec4(r, g, b, a / 255)
        end
        local color = type(color) == 'string' and tonumber(color, 16) or color
        if type(color) ~= 'number' then return end
        local r, g, b, a = explode_argb(color)
        return imgui.ImColor(r, g, b, a):GetVec4()
    end

    local render_text = function(text_)
        for w in text_:gmatch('[^\r\n]+') do
            local text, colors_, m = {}, {}, 1
            w = w:gsub('{(......)}', '{%1FF}')
            while w:find('{........}') do
                local n, k = w:find('{........}')
                local color = getcolor(w:sub(n + 1, k - 1))
                if color then
                    text[#text], text[#text + 1] = w:sub(m, n - 1), w:sub(k + 1, #w)
                    colors_[#colors_ + 1] = color
                    m = n
                end
                w = w:sub(1, n - 1) .. w:sub(k + 1, #w)
            end
            if text[0] then
                for i = 0, #text do
                    imgui.TextColored(colors_[i] or colors[1], u8(text[i]))
                    imgui.SameLine(nil, 0)
                end
                imgui.NewLine()
            else imgui.Text(u8(w)) end
        end
    end

    render_text(text)
end


function checkBankInterior()
    for id = 0, 2048 do
        if sampIs3dTextDefined(id) then
            local text2, color2, posX2, posY2, posZ2, distance2, ignoreWalls2, player2, vehicle2 = sampGet3dTextInfoById(id)
            if text2:find('Рабочее место: Касса 2') then
                return true
            end
        end
    end
end